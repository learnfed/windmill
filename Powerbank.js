import Render from './Render'
import Status from './Status'
import Reliability from './Reliability'

class Powerbank extends Render {
    constructor(id, capacity, initialCharge){
        super(id);
        this._capacity = capacity;
        this._currentCharge = initialCharge;

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Powerbank status:';
        this._parentId.appendChild(h2);
        this._pCapacity = document.createElement('P');
        this._pCurrentCharge = document.createElement('P');
    
        this._parentId.appendChild(this._pCapacity);
        this._parentId.appendChild(this._pCurrentCharge);
        
        this._pCapacity.innerHTML = `Capacity: ${this._capacity}`;
        this._update();
        
    }

    _update(){
        this._pCurrentCharge.innerHTML = `Current charge: ${this._currentCharge}`;
    }

    powerDraw(inout,reliabilityObject) {

        this._currentCharge += inout;
        if(this._currentCharge > this._capacity) {
            this._currentCharge = this._capacity;
            Status.Warning('Energy waste');
            reliabilityObject.waste();
        } else if(this._currentCharge < 0){
            this._currentCharge = 0;
            Status.Error('Insufficient power');
            reliabilityObject.outage();
        } else {
            Status.Ok();
            reliabilityObject.ok();
        }
        this._update();
    }

    get currentCharge(){
        return this._currentCharge;        
    }

}
export default Powerbank
