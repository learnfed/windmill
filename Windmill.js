/**
 * Maintained by IldikoR
 */
import Render from './Render'
import WINDP from './Windpower';


class Windmill extends Render {
    constructor(windLight, windStrong, id) {
        super(id);
        this._windLight = windLight;
        this._windStrong = windStrong;
        this._currentOutput = null;

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Windmill status:';
        this._parentId.appendChild(h2);
        this._pMaxPower = document.createElement('P');
        this._pMinPower = document.createElement('P');
        this._pCurrentPower = document.createElement('P');
        this._parentId.appendChild(this._pMaxPower);
        this._parentId.appendChild(this._pMinPower);
        this._parentId.appendChild(this._pCurrentPower);
        this._pMaxPower.innerHTML = `Maximal power output: ${this._windLight}`;
        this._pMinPower.innerHTML = `Minimal power output: ${this._windStrong}`;
        this._pCurrentPower.innerHTML = `Current power output: ${this._currentOutput}`;
        
    }

    _update() {
        this._pCurrentPower.innerHTML = `Current power output: ${this._currentOutput}`;
    }

    output(windSpeed) {
        switch(windSpeed) {
            case WINDP.NONE: this._currentOutput = 0; break;
            case WINDP.LIGHT: this._currentOutput = this._windLight; break;
            case WINDP.STRONG: this._currentOutput = this._windStrong; break;
            case WINDP.HURRICANE: this._currentOutput = 0; break;
            default: throw new Error('Not a valid wind speed value.');
        }
        this._update();
        return this._currentOutput;
    }
}

export default Windmill;
