import Render from './Render';
import Chart from 'chart.js';

class Graph extends Render {
    constructor(id,initCharge){
        super(id);

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Current Voltage - Time diagram';
        this._parentId.appendChild(h2);

        const canv = document.createElement('Canvas');
        canv.setAttribute('id','myChart');
        this._parentId.appendChild(canv);

        this._myChart = new Chart(canv, {
            type: 'line',
            data: {
                labels: [0],
                datasets: [{
                    label: 'Powerbank wattage',
                    data: [initCharge],
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 2
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 1000
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            min: 1,
                            max: 24,
                        }
                    }]
                }
            }
        });
    }

    setData(time,wattage){
        this._myChart.data.datasets[0].data.push(wattage);
        this._myChart.data.labels.push(time);
        this._myChart.update();
    }

}

export default Graph
