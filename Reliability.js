/**
 * Maintained by Misu
 */

import Render from './Render';


class Reliability extends Render {
    constructor(id) {
        super(id);
        this._outage = 0;
        this._ok = 0;
        this._waste = 0;

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Reliability status:';
        this._parentId.appendChild(h2);
        this._pOutage = document.createElement('P');
        this._pOk = document.createElement('P');
        this._pWaste = document.createElement('P');
        this._parentId.appendChild(this._pOutage);
        this._parentId.appendChild(this._pOk);
        this._parentId.appendChild(this._pWaste);

        this._initialUpdate();
    }

    casePercentage (divident, totalCases) {
        return (divident/totalCases*100).toFixed(2);
    }

    _update() {
        const totalCases = this._outage + this._ok + this._waste;

        const outagePerc = this.casePercentage(this._outage, totalCases);
        const okPerc = this.casePercentage(this._ok, totalCases);
        const wastePerc = this.casePercentage(this._waste, totalCases);

        this._pOutage.innerHTML = `An outage happened ${this._outage} times. (${outagePerc}%)`;
        this._pOk.innerHTML = `The generated power was equal to the consumption ${this._ok} times. (${okPerc}%)`;
        this._pWaste.innerHTML = `Überflüssig power was generated ${this._waste} times. (${wastePerc}%)`;
    }

    _initialUpdate() {
        this._pOutage.innerHTML = `An outage happened 0 times. (0%)`;
        this._pOk.innerHTML = `The generated power was equal to the consumption 0 times. (0%)`;
        this._pWaste.innerHTML = `Überflüssig power was generated 0 times. (0%)`;
    }

    reset() {
        this._outage = 0;
        this._ok = 0;
        this._waste = 0;

        this._initialUpdate();
    }



    outage() {
        this._outage++;
        this._update();
    }

    ok() {
        this._ok++;
        this._update();
    }

    waste() {
        this._waste++;
        this._update();
    }

}

export default Reliability
