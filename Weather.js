/**
 * Maintained by Benji
 */
import Render from './Render';
import WINDP from './Windpower';

class Weather extends Render {
    constructor(id) {
        super(id);
        this._hour = 0;
        this._windPower = WINDP.NONE;

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Weather:';
        this._parentId.appendChild(h2);
        this._pWindPower = document.createElement('P');
        this._pTime = document.createElement('P');
        this._parentId.appendChild(this._pWindPower);
        this._parentId.appendChild(this._pTime);
        this._update();
    }

    _update() {
        this._pWindPower.innerHTML = `Wind intensity: ${this._windPower.description}`;
        this._pTime.innerHTML = `Time: ${this._hour}`;
    }

    tick(){

        let chance = Math.random();

        switch(this._windPower){
            case WINDP.NONE:
                if (chance >= 0.2) {
                    this._windPower = WINDP.LIGHT;
                } break;
            case WINDP.LIGHT:
                if (chance <= 0.2) {
                    this._windPower = WINDP.NONE;
                } else if (chance >= 0.6) {
                    this._windPower = WINDP.STRONG;
                } break;
            case WINDP.STRONG:
                if (chance <= 0.6) {
                    this._windPower = WINDP.LIGHT;
                } else if (chance >= 0.9) {
                    this._windPower = WINDP.HURRICANE;
                } break;
            case WINDP.HURRICANE:
                if (chance >= 0.2) {
                    this._windPower = WINDP.STRONG;
                } break;
        }

        this._hour++;
        if (this._hour == 24) {
            this._hour = 0
        }

        // console.log(`hour: ${this._hour} wind: ${this._windPower}`);

        this._update();

       return this._windPower;
    }

    get time(){
        return this._hour;
    }


}
export default Weather
