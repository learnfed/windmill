class Render {
    constructor(parentId) {
        this._parentId = document.getElementById(parentId);
    }

    _update() {
        throw new Error('Default render update method was not overwritten!')
    }
}

export default Render;
