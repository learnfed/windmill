/**
 * Maintained by IldikoR
 */

/**
 * House class
 * @param: name - type
 * @param: size - no of persons
 * @param:  consumptionGrade - consumption per person
 */

class House {
    constructor(name, size, consumptionGrade) {
        this._name = name;
        this._size = size;
        this._consumptionGrade = consumptionGrade;
        this._consumption = size * consumptionGrade;
    }
    get consumption() {return this._consumption};

    get name() {return this._name};
}
export default House;
