import Village from './Village';
import House from './House';
import Windmill from './Windmill';
import Weather from './Weather';
import Status from './Status';
import Reliability from './Reliability';
import Powerbank from './Powerbank';
import Graph from './Graph';




const village = new Village('Scara brae', 'village');
village.build(new House('household', 4, 1));
village.build(new House('school', 30, 2));
village.build(new House('hospital', 25, 3));

const windmill = new Windmill(100, 300, 'windmill');

const weather = new Weather('weather');

const reliability = new Reliability('reliability');

const initCharge = 500;

const powerbank = new Powerbank('powerbank', 1000, initCharge);

const graph = new Graph('graph', initCharge);

const application = () => {
    const windmillOutput = windmill.output(weather.tick());
    const villageCons = village.calculateCons();

    powerbank.powerDraw(windmillOutput-villageCons,reliability);

    graph.setData(weather.time,powerbank.currentCharge);

}

const step = document.getElementById('step');
step.addEventListener('click', application);

const resetRelBtn = document.getElementById('resetRel');
resetRelBtn.addEventListener('click', () => {
    reliability.reset();
});

const startStop = document.getElementById('start-stop');
let timer;
startStop.addEventListener('click', () => {
        if(step.disabled) { //stop setInterval
            step.disabled = false;
            startStop.innerHTML = 'Start';
            clearInterval(timer);
        } else { //to start setInterval
            step.disabled = true;
            startStop.innerHTML = 'Stop';
            timer = setInterval(application, 500)
        }
    }
);

let current = 'https://i.imgur.com/mN2dvId.png';
const logoPicture = document.getElementById('logo');
logoPicture.addEventListener('click',() => {
    switch(current){
        case 'https://i.imgur.com/mN2dvId.png':
            logoPicture.setAttribute('src','https://i.imgur.com/5KuMHSf.png');
            current = 'https://i.imgur.com/5KuMHSf.png';
        break;
        case 'https://i.imgur.com/5KuMHSf.png':
            logoPicture.setAttribute('src','https://i.imgur.com/NYXqUoO.png');
            current = 'https://i.imgur.com/NYXqUoO.png';
        break;
        case 'https://i.imgur.com/NYXqUoO.png':
            logoPicture.setAttribute('src','https://i.imgur.com/mN2dvId.png');
            current = 'https://i.imgur.com/mN2dvId.png';
        break;
    }
})