class Status {
    
    static Ok(message=''){
        const parent = document.getElementById('status');
        parent.innerHTML = `<p>Status: OK<br> Message: ${message}</p>`
    }
    
    static Warning(message=''){
        const parent = document.getElementById('status');
        parent.innerHTML = `<p>Status: Warning<br> Message: ${message}</p>`
    }
    
    static Error(message=''){
        const parent = document.getElementById('status');
        parent.innerHTML = `<p>Status: Error <br> Message: ${message}</p>`
    }
}

export default Status
