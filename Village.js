/**
 * Maintained by Benji
 * @alias 
 * @param {string} name  The name of the village
 * 
 */

import Render from './Render'

class Village extends Render{
    constructor(name, id){
        super(id);
        this._name = name;
        this._buildings = [];
        this._powerCons = null;

        const h2 = document.createElement('H2');
        h2.innerHTML = 'Village details';
        this._parentId.appendChild(h2);
        this._pName = document.createElement('P');
        this._pConsumption = document.createElement('P');
        this._pHouseList = document.createElement('UL');
        this._pHouseList.innerHTML = '<h3>List of buildings</h3>';
        this._parentId.appendChild(this._pName);
        this._parentId.appendChild(this._pConsumption);
        this._parentId.appendChild(this._pHouseList);
        this._pName.innerHTML = `Name of the village: ${this._name}`;
        this._pConsumption.innerHTML = `Power consumption: ${this._powerCons}`;
        

        
    }

    _houseListInsert(name) {
        const item = document.createElement('LI');
        item.innerHTML = name;
        this._pHouseList.appendChild(item);
    }

    build(house){
        this._buildings.push(house);
        this._houseListInsert(house.name)
    }

    calculateCons(){
        let totalCons = 0;
        this._buildings.forEach(building => {
            totalCons += building.consumption;
        })
        this._powerCons = totalCons;
        this._pConsumption.innerHTML = `Power consumption: ${this._powerCons}`;
        return totalCons;
    }
}

export default Village;
