const WINDP = {
    NONE: Symbol('none'),
    LIGHT: Symbol('light'),
    STRONG: Symbol('strong'),
    HURRICANE: Symbol('hurricane')
}

export default WINDP
